#!/bin/bash

#requires variable: 'SSH_KEY', a base64 encoding of a private ssh key

set -e
set -u

tempdirectory=$(mktemp -d)

output="/builds/tarbe/web_public/output"


declare -r KNOWN_HOST=$tempdirectory/known_hosts
echo "|1|Qhh4MOKPf5hGV6frTOWJm7nNpKY=|Wlr1Jvfx32Nko4XN673w12blw4Y= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBBtalHpVlexWSN5JZ7Lf1TE8EUXe+O80NlgWp436gzHsM74+kvp6a+q+/KdgC886WqOrX9XLDRniEDXEaLDlZx0=" >> $KNOWN_HOST
echo "|1|P+UC6g05LcdBoCknApOdIul9dXk=|X4WX99CtWUp0aoVRHDdQXU5Sh/M= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBBtalHpVlexWSN5JZ7Lf1TE8EUXe+O80NlgWp436gzHsM74+kvp6a+q+/KdgC886WqOrX9XLDRniEDXEaLDlZx0=" >> $KNOWN_HOST

declare -r ID_RSA_FILE=$tempdirectory/id_rsa
echo $SSH_KEY | base64 -di > $ID_RSA_FILE
chmod 0400 $ID_RSA_FILE
ssh-keygen -y -f $ID_RSA_FILE
tar cpf - $output | ssh -v -i $ID_RSA_FILE -oBatchMode=yes -o UserKnownHostsFile=$KNOWN_HOST -o IdentitiesOnly=yes $httpserver "tar xpf - -C ." 
rm -f $ID_RSA_FILE
