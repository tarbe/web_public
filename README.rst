Site web public de l'amicale
============================

Il s'agit d'un site «statique», c'est à dire très simple.

Technique
----------

.. _pelican http://docs.getpelican.com/
.. _framagit https://framagit.org/tarbe/web_public
.. _rst http://www.sphinx-doc.org/en/stable/rest.html

* Site réalisé avec pelican_, 
* géré sur framagit_,
* langage utilisé : rst_ (restructured text).
