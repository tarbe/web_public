#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u"Amicale de l'\xc9cole Maternelle Cours Tarb\xe9"
SITENAME = u"Amicale de l'\xc9cole Maternelle Cours Tarb\xe9"
SITEURL = ''
PLUGINS = ('pelican_alias',)

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ("Site académique de l'école",
     'https://www.education.gouv.fr/annuaire/89-yonne/sens/etab/ecole-maternelle-cours-tarbe.html'
     ),
    ("Archives de la liste e-mail «actifs»",
     'https://framalistes.org/sympa/arc/amicale_cours_tarbe_actifs/2018-01/'),)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

PIWIK_URL = 'analytics.alwaysdata.com'
PIWIK_SITE_ID = '142928'

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
