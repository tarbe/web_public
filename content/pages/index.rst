À propos
============================================

:date: 2018-01-19 11:33
:tags: amicale,accueil
:category: accueil
:author: Feth AREZKI
:lang: fr
:slug: a_propos
:status: published

.. raw:: html

        <script type="text/javascript">
            function gen_mail_to_link(lhs,rhs,subject) {
                document.write("<a href=\"mailto");
                document.write(":" + lhs + "@");
                document.write(rhs + "?subject=" + subject + "\">" + lhs + "@" + rhs + "<\/a>");
            }
        </script>

Vocation
----------

Appuyer l'école dans ses missions.
Au sens large, toute action qui profite aux enfants est de notre ressort !

Participer
-----------

Les parents et les professeurs sont membres de droit de l'association.

Nous contacter
--------------


.. raw:: html

        Trouvez-nous à l'école, ou bien écrivez-nous un e-mail :
        <script type="text/javascript"> 
            gen_mail_to_link('contact','tarbe.fr','Prise de contact');
        </script>
        <noscript>
          <em>Écrivez à contact chez tarbe.fr.</em>
        </noscript>



Adresse de l'école
-------------------

24 Cours Tarbé

89100 Sens

.. raw:: html

        <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=3.2777103781700134%2C48.19393736817844%2C3.2821896672248845%2C48.19551077607011&amp;layer=mapnik&amp;marker=48.194724078163794%2C3.2799500226974487" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=48.19472&amp;mlon=3.27995#map=19/48.19472/3.27995">Afficher une carte plus grande</a></small>

