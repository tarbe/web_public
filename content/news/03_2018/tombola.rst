Tombola du printemps : vente des tickets
=========================================

:date: 2018-03-19 11:00
:tags: amicale
:category: actions
:author: Feth AREZKI
:lang: fr
:slug: tombola_2018
:status: published

Vous avez reçu deux carnets de 5 tickets de tombola dans une enveloppe, ainsi que le règlement (cliquer pour agrandir) :

.. raw:: html

  <a class="reference external image-reference" href="images/flyer_tombola_2018.jpg">
  <img alt="tract tombola 2018 - mode d'emploi" class="align-center" src="images/flyer_tombola_2018.jpg" style="width: 30%; display: block; margin-left: auto; margin-right: auto;"" />
  </a>

Nous vous sollicitons pour acheter ces tickets et les vendre à vos proches (famille, amis, voisins …) au prix de 1 euro le ticket.

Si vous avez besoin de tickets supplémentaires, nous en laisserons à votre disposition auprès des enseignant·e·s.

Retour des tickets
--------------------

Au plus tard le *mercredi 4 avril 2018*, vous déposerez dans la classe de votre enfant vos tickets et l'argent obtenu dans l'enveloppe jointe.

Tirage au sort
---------------

Le tirage au sort aura lieu le *vendredi 6 avril* avec les élèves et en compagnie de l’amicale pour les petits lots et *à 16h15* en public pour les gros lots.

La liste des gagnants restera bien sûr affichée le temps de remettre les lots à leurs gagnants

Lots à gagner
---------------

Tablette tactile samsung, hoverboard, VTT 24 pouces, bons d'achats divers (jouets, restaurants, sport, soins en institut de beauté...) et de nombreux petits lots.

.. class:: center

  *Bonne chance !*
