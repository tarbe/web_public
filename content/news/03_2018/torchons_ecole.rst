Torchons !
=============

:date: 2018-03-16 11:00
:tags: amicale,dessins
:category: actions
:author: Feth AREZKI
:lang: fr
:slug: torchons_2018
:status: published

Les torchons arrivent !
------------------------------

Les enfants les ont dessinés, nous les avons commandés …

les torchons, les linges de cuisine, les pattemouilles -comment les appelez-vous ?- arrivent cette semaine !

Le tissu sera imprimé en blanc sur fond bleu marine.

.. image:: images/torchons_dessins_2018.jpg
  :alt: bonshommes dessinés par tous les enfants de l'école assemblés
  :width: 100%
  :target: images/torchons_dessins_2018.jpg
