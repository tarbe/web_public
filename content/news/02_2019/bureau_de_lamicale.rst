Amicale 2018-2019
===================

:date: 2019-02-04 11:00
:tags: amicale,bureau
:category: reunions
:author: Emmeline Carrier
:lang: fr
:slug: bureau_1819
:status: published


.. image:: images/flyer_amicale_201819.jpg
  :alt: dessin de présentation de l'amicale
  :width: 100%
  :target: images/flyer_amicale_201819.jpg

Composition du bureau 2018 2019
--------------------------------

* Président : Feth AREZKI (parent classe 6)
* Présidents adjoints : Olivier DEVOIZE, Adrien RICHARD et Anaïs TURHAN (parents classe 8)
* Trésorière : Sophie LONJARET (maîtresse classe 7)
* Adjoint trésorier : Nicolas POTIER (parent classe 4)
* Secrétaire : Lucie HAIGNERE (Directrice et maîtresse classe 6)
* Secrétaires adjointes : Emmy CARRIER (parent classe 3 et 8) et Cécile SENERS (parent classe 6 et 8)

Et beaucoup de membres actifs !


.. image:: images/trombi_201819.jpg
  :alt: trombi de membres actifs
  :width: 100%
  :target: images/trombi_201819.jpg


Qu’est-ce que l’amicale ?
--------------------------

* L’amicale est une association Loi 1901 à but non lucratif. Tous les parents ayant un enfant dans l’école sont membres de droit, ainsi que le corps enseignant.
* Elle est composée d’un bureau, ainsi que des membres actifs, personnes indispensables à l’activité de l’amicale.
* L’amicale et la coopérative scolaire ont fusionné cette année 2018.
* L’amicale apporte son soutien sur différents projets au cours de l’année (fête de fin d’année, sorties scolaires, venue du père noël, goûters, achats de matériels pour l’école, etc).

Comment fonctionne l’amicale ?
------------------------------


* L’amicale fonctionne essentiellement avec de bonnes volontés, mais aussi grâce aux dons qui sont faits à l’association, des bénéfices récoltés lors de diverses actions organisées par l’Amicale au sein de l’école au cours de l’année. 

* Pour faire vivre votre amicale, nous avons besoin de vous, de vos idées ainsi que de vos suggestions. L’association se réunit plusieurs fois dans l’année, cela vous annoncé sur le tableau d’affichage (à côté du portail) et également dans le carnet de liaison de votre enfant.


N’hésitez pas à venir nous rejoindre !!
