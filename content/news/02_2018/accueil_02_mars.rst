Petit goûter du Cours Tarbé
============================

:date: 2018-02-10 11:00
:tags: amicale,gouter,crepes
:category: animations
:author: Feth AREZKI
:lang: fr
:slug: gouter_02_mars_2018
:status: published

Une crêpe offerte à chaque enfant de l'école !
----------------------------------------------

.. image:: /images/gateau_jus_crepe.png
  :alt: gâteau, jus de fruits, crêpes


Vos gâteaux faits maison sont les bienvenus !


Où ; quand
----------

* Dans l'école du Cours Tarbé
* après midi du 02 mars 2018, de la fin de la classe à la fin du périscolare

Le carton d'invitation
-----------------------

.. image:: /images/invitation_gouter_02_mars_2018.png
  :alt: carton d'invitation
