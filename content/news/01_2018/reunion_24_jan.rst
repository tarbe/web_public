Réunion le 24 janvier
=====================

:date: 2018-01-22 11:00
:tags: amicale,réunion
:category: réunions
:author: Feth AREZKI
:lang: fr
:slug: reu_24_jan_2018
:status: published

.. image:: /images/invitation_reu_01_2018.png
  :alt: affiche d'invitation

Où ; quand
----------

* Dans l'école du Cours Tarbé
* 24 janvier 2018, 18 h 30

Ordre du jour prévisionnel
---------------------------

- tombola:

  - organisation de la collecte de lots auprès de commerçants
  - date (6 avril ?)

- kermesse:

  - organisation au sens large
  - date

- recruter des membres (tableau etc)

- point fonctionnement

  - site internet/listes de discussion etc
  - finances
